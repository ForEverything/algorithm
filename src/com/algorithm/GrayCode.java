package com.algorithm;

import java.util.*;

/**
 * @since 20160409
 在一组数的编码中，若任意两个相邻的代码只有一位二进制数不同， 则称这种编码为格雷码(Gray Code)，请编写一个函数，使用递归的方法生成N位的格雷码。
给定一个整数n，请返回n位的格雷码，顺序为从0开始。
测试样例：
1
返回：["0","1"]

 * */
public class GrayCode {
	
	
	public static String[] getGray(int n) {
		String[] gray = new String[(int) Math.pow(2, n)];
		if (n == 1) {
			gray[0] = "0";
			gray[1] = "1";
			return gray;
		}
		String[] last = getGray(n - 1);
		for (int i = 0; i < last.length; i++) {
			gray[i] = "0" + last[i];
			gray[gray.length - 1 - i] = "1" + last[i];
		}
		return gray;
	}
	
	

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		while (input.hasNext()) {
			int n = input.nextInt();
			String[] res = getGray(n);
			for (int i = 0; i < res.length; i++) {
				System.out.println(res[i]);
			}
		}

	}

}