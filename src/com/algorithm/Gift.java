package com.algorithm;

import java.util.*;

/**
@since 20160409️
春节期间小明使用微信收到很多个红包，非常开心。在查看领取红包记录时发现，某个红包金额出现的次数超过了红包总数的一半。请帮小明找到该红包金额。写出具体算法思路和代码实现，要求算法尽可能高效。
给定一个红包的金额数组gifts及它的大小n，请返回所求红包的金额。
测试样例：
[1,2,3,2,2],5
返回：2
 
**/
public class Gift {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
         
		//int i=input.nextInt();	
		//while (input.hasNext()) {
		//	int n = input.nextInt();
		//	int[] res = new int[n];
		//	for (int i = 0; i < n; i++) {
		//		int m = input.nextInt();
		//		res[i] = m;
		//	}
		int i[]={2,4,3,2,3,3,3,3,7};
		 System.out.println(getValue(i, 9));
		//}
	}
	public static int getValue(int[] gifts, int n) {
		Arrays.sort(gifts);
		int count = 0;
		int res = 0;
		for (int i = 0; i < n; i++) {
			if (i < n - 1 && gifts[i] == gifts[i + 1]) {
				count = count + 1;
			} else {
				count = 0;
			}
			if (count >= n / 2) {
				res = gifts[i];
				break;
			}
		}
		return res;
	}
}